# Recommendation System 

This project is a training in recommendation classification problems

## The project has 2 parts
* Classical ML approach 
* Deep Learning approach 

## Problem I am trying to solve
Recommend posts to users based on the history of their post likes 

## Classical ML approach 
* Feature engineering logic is stored in `Feature_engineering` notebook.
* Posts' texts are embedded via tf-idf vectorizer. I apply K-means clustering to Tf-idf based features. 
* CatBoost model is trained with accuracy ~65% on test dataset. 

## Deep Learning approach 
TBD 