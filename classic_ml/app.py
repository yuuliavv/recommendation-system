from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from typing import List
from fastapi import FastAPI
from datetime import datetime
from pydantic import BaseModel
import pandas as pd
import os
from catboost import CatBoostClassifier

dbname = os.environ.get("DB_NAME")
host = os.environ.get("HOST")
user = os.environ.get("DB_USER")
pw = os.environ.get("PASSWORD")

SQLALCHEMY_DATABASE_URL = f"postgresql://{user}:{pw}@{host}:6432/{dbname}"

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
app = FastAPI()


def get_db():
    with SessionLocal() as db:
        return db


class PostGet(BaseModel):
    id: int
    text: str
    topic: str

    class Config:
        orm_mode = True


def get_model_path(path: str) -> str:
    if os.environ.get("IS_LMS") == "1":
        MODEL_PATH = '/workdir/user_input/model'
    else:
        MODEL_PATH = path
    return MODEL_PATH


def load_models():
    model_path = get_model_path("model")
    from_file = CatBoostClassifier()
    model = from_file.load_model(model_path)
    return model


def batch_load_sql(query: str) -> pd.DataFrame:
    CHUNKSIZE = 200000
    engine = create_engine(
        f"postgresql://{user}:{pw}@"
        f"{host}:6432/{dbname}"
    )
    conn = engine.connect().execution_options(stream_results=True)
    chunks = []
    for chunk_dataframe in pd.read_sql(query, conn, chunksize=CHUNKSIZE):
        chunks.append(chunk_dataframe)
    conn.close()
    return pd.concat(chunks, ignore_index=True)


def load_features():
    post_features_engineered = batch_load_sql('SELECT * FROM yulia_volkova_features')
    return post_features_engineered


model = load_models()

my_features_engineered = load_features()
liked_posts_query = """
        SELECT distinct post_id, user_id
        FROM public.feed_data
        where action ='like'"""

all_user_features = batch_load_sql('SELECT * FROM public.user_data')

app = FastAPI()
liked_posts_mine = batch_load_sql(liked_posts_query)


# Call endpoint and return post recommendation for a user.

def get_recommended_feed(
        id: int,
        time: datetime,
        limit: int = 10):
    user_features = all_user_features.loc[all_user_features['user_id'] == id]
    user_features = user_features.drop('user_id', axis=1)
    liked_posts = liked_posts_mine[liked_posts_mine.user_id == id].post_id.values

    posts_features = my_features_engineered.drop(['index', 'text'], axis=1)
    content = my_features_engineered[['post_id', 'text', 'topic']]
    add_user_features = dict(zip(user_features.columns, user_features.values[0]))
    user_posts_features = posts_features.assign(**add_user_features)
    user_posts_features = user_posts_features.set_index('post_id')

    user_posts_features['hour'] = time.hour
    user_posts_features['month'] = time.month

    predicts = model.predict_proba(user_posts_features)[:, 1]
    user_posts_features["predicts"] = predicts
    # remove pots which were already liked
    filtered_ = user_posts_features[~user_posts_features.index.isin(liked_posts)]

    recommended_posts = filtered_.sort_values('predicts')[-limit:].index
    return [
        PostGet(**{
            "id": i,
            "text": content[content.post_id == i].text.values[0],
            "topic": content[content.post_id == i].topic.values[0]
        }) for i in recommended_posts
    ]


@app.get("/post/recommendations/", response_model=List[PostGet])
def recommended_posts(id: int, time: datetime, limit: int = 10) -> List[PostGet]:
    return get_recommended_feed(id, time, limit)
